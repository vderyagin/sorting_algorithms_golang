// Bubble sort
//
// Time complexity:
//     O(n^2)
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     yes
//
// Algorithm that works by repeatedly stepping through the list to be sorted,
// comparing each pair of adjacent items and swapping them if they are in the
// wrong order. The pass through the list is repeated until no swaps are
// needed, which indicates that the list is sorted.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Bubble_sort)

package main

import "fmt"

func bubbleSort(list []int) {
	if len(list) < 2 {
		return
	}

	swapped := true

	for l := list; swapped && len(l) > 1; l = l[:len(l)-1] {
		swapped = false
		for idx := range l[:len(l)-1] {
			if l[idx] > l[idx+1] {
				l[idx], l[idx+1] = l[idx+1], l[idx]
				swapped = true
			}
		}
	}
}

func main() {
	l := []int{}
	bubbleSort(l)
	fmt.Println(l)

	l = []int{2}
	bubbleSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	bubbleSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	bubbleSort(l)
	fmt.Println(l)
}
