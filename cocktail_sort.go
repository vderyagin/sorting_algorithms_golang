// Cocktail sort
//
// Time complexity:
//     O(n^2)
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     yes
//
// The algorithm differs from a bubble sort in that it sorts in both
// directions on each pass through the list. This sorting algorithm is only
// marginally more difficult to implement than a bubble sort, and solves the
// problem of turtles in bubble sorts.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Cocktail_sort)

package main

import "fmt"

func cocktailSort(list []int) {
	if len(list) < 2 {
		return
	}

	swapped := true
	l := list

	for len(l) > 1 {
		swapped = false
		for idx := range l[:len(l)-1] {
			if l[idx] > l[idx+1] {
				l[idx], l[idx+1] = l[idx+1], l[idx]
				swapped = true
			}
		}

		if !swapped {
			break
		}

		l = l[:len(l)-1]

		swapped = false
		for idx := len(l) - 1; idx > 0; idx-- {
			if l[idx] < l[idx-1] {
				l[idx], l[idx-1] = l[idx-1], l[idx]
				swapped = true
			}
		}

		if !swapped {
			break
		}

		l = l[1:len(l)]
	}
}

func main() {
	l := []int{}
	cocktailSort(l)
	fmt.Println(l)

	l = []int{2}
	cocktailSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	cocktailSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	cocktailSort(l)
	fmt.Println(l)
}
