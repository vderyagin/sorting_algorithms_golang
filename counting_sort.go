// Counting sort
//
// Time complexity:
//     O(n)
//
// Space complexity:
//     O(n + k) k - range of sorted elements
//
// Stability:
//     yes
//
// Counting sort operates by counting the number of objects that have each
// distinct key value, and using arithmetic on those counts to determine the
// positions of each key value in the output sequence.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Counting_sort)

package main

import "fmt"

func countingSort(list []int) []int {
	if len(list) < 2 {
		return list
	}

	count := make([]int, 100) // can not sort lists with elements not in [0..99]

	for _, elem := range list {
		count[elem]++
	}

	total := 0

	for idx, elem := range count {
		if elem != 0 {
			oldCount := elem
			count[idx] = total
			total += oldCount
		}
	}

	out := make([]int, len(list))

	for _, item := range list {
		out[count[item]] = item
		count[item]++
	}

	return out
}

func main() {
	l := []int{}
	sorted := countingSort(l)
	fmt.Println(sorted)

	l = []int{2}
	sorted = countingSort(l)
	fmt.Println(sorted)

	l = []int{2, 1}
	sorted = countingSort(l)
	fmt.Println(sorted)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	sorted = countingSort(l)
	fmt.Println(sorted)
}
