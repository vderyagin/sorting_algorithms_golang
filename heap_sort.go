// Heap sort
//
// Time complexity:
//     O(n log n)
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     no
//
// The heapsort algorithm can be divided into two parts.
//
// In the first step, a heap is built out of the data.
//
// In the second step, a sorted array is created by repeatedly removing the
// largest (smallest) element from the heap, and inserting it into the
// array. The heap is reconstructed after each removal. Once all objects have
// been removed from the heap, we have a sorted array. The direction of the
// sorted elements can be varied by choosing a min-heap or max-heap in step
// one.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Heapsort)

package main

import "fmt"

func childrenIdxs(heap []int, idx int) []int {
	children := make([]int, 0, 2)

	xIdx := idx*2 + 1
	yIdx := idx*2 + 2

	if xIdx < len(heap) {
		children = append(children, xIdx)
	}

	if yIdx < len(heap) {
		children = append(children, yIdx)
	}

	return children
}

func hasBiggerChild(heap []int, idx int) bool {
	if idx >= len(heap) {
		return false
	}

	children := childrenIdxs(heap, idx)

	for _, i := range children {
		if heap[i] > heap[idx] {
			return true
		}
	}

	return false
}

func switchWithBiggestChild(heap []int, idx int) (newIdx int) {
	newIdx = idx

	for _, i := range childrenIdxs(heap, idx) {
		if heap[i] > heap[newIdx] {
			newIdx = i
		}
	}

	heap[idx], heap[newIdx] = heap[newIdx], heap[idx]

	return newIdx
}

func bubbleDown(heap []int, idx int) {
	if hasBiggerChild(heap, idx) {
		bubbleDown(heap, switchWithBiggestChild(heap, idx))
	} else {
		return
	}
}

func heapSort(list []int) {
	heap := list

	for i := (len(heap) - 2) / 2; i >= 0; i-- {
		bubbleDown(heap, i)
	}

	for len(heap) > 1 {
		heap[0], heap[len(heap)-1] = heap[len(heap)-1], heap[0]
		heap = heap[:len(heap)-1]
		bubbleDown(heap, 0)
	}
}

func main() {
	l := []int{}
	heapSort(l)
	fmt.Println(l)

	l = []int{2}
	heapSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	heapSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	heapSort(l)
	fmt.Println(l)
}
