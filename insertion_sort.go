// Insertion sort
//
// Time complexity:
//     O(n^2)
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     yes
//
// Insertion sort iterates, consuming one input element each repetition, and
// growing a sorted output list. On a repetition, insertion sort removes one
// element from the input data, finds the location it belongs within the
// sorted list, and inserts it there. It repeats until no input elements
// remain.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Insertion_sort)

package main

import "fmt"

func insertionSort(list []int) {
	if len(list) == 0 {
		return
	}

	sorted := list[:1]
	unsorted := list[1:]

	for len(unsorted) > 0 {
		elem := unsorted[0]

		unsorted = unsorted[1:]
		sorted = append(sorted, 0)

		i := len(sorted) - 2
		for ; i >= 0; i-- {
			if sorted[i] > elem {
				sorted[i+1] = sorted[i]
			} else {
				break
			}
		}

		sorted[i+1] = elem
	}
}

func main() {
	l := []int{}
	insertionSort(l)
	fmt.Println(l)

	l = []int{2}
	insertionSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	insertionSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	insertionSort(l)
	fmt.Println(l)
}
