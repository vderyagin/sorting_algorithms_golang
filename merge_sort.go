// Merge sort
//
// Time complexity:
//     O(n log n)
//
// Space complexity:
//     O(n)
//
// Stability:
//     yes
//
// Conceptually, a merge sort works as follows:
// Divide the unsorted list into n sublists, each containing 1 element (a list
// of 1 element is considered sorted) and then repeatedly merge sublists to produce
// new sublists until there is only 1 sublist remaining. This will be the
// sorted list.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Merge_sort)

package main

import "fmt"

func mergeSort(list []int) []int {
	if len(list) <= 1 {
		return list
	}

	middle := len(list) / 2
	left := mergeSort(list[:middle])
	right := mergeSort(list[middle:])

	result := make([]int, 0, len(list))

	for len(left) != 0 && len(right) != 0 {
		if left[0] <= right[0] {
			result = append(result, left[0])
			left = left[1:]
		} else {
			result = append(result, right[0])
			right = right[1:]
		}
	}

	result = append(result, left...)
	result = append(result, right...)

	return result
}

func main() {
	l := []int{}
	sorted := mergeSort(l)
	fmt.Println(sorted)

	l = []int{2}
	sorted = mergeSort(l)
	fmt.Println(sorted)

	l = []int{2, 1}
	sorted = mergeSort(l)
	fmt.Println(sorted)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	sorted = mergeSort(l)
	fmt.Println(sorted)
}
