// Quicksort
//
// Time complexity:
//     O(n log n) best, average
//     O(n^2)     worst
//
// Space complexity:
//     O(n)
//
// Stability:
//     no
//
// Quicksort first divides a large list into two smaller sub-lists: the low
// elements and the high elements. Quicksort can then recursively sort the
// sub-lists.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Quicksort)

package main

import "fmt"

func Quicksort(list []int) []int {
	if len(list) <= 1 {
		return list
	}

	pivot := list[0]

	low := make([]int, 0, len(list)-1)
	high := make([]int, 0, len(list)-1)
	result := make([]int, 0, len(list))

	for _, elem := range list[1:] {
		if elem < pivot {
			low = append(low, elem)
		} else {
			high = append(high, elem)
		}
	}

	result = append(result, Quicksort(low)...)
	result = append(result, pivot)
	result = append(result, Quicksort(high)...)

	return result
}

func main() {
	l := []int{}
	sorted := Quicksort(l)
	fmt.Println(sorted)

	l = []int{2}
	sorted = Quicksort(l)
	fmt.Println(sorted)

	l = []int{2, 1}
	sorted = Quicksort(l)
	fmt.Println(sorted)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	sorted = Quicksort(l)
	fmt.Println(sorted)
}
