// Selection sort
//
// Time complexity:
//     O(n^2)
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     no
//
// The algorithm divides the input list into two parts: the sublist of items
// already sorted, which is built up from left to right at the front (left) of
// the list, and the sublist of items remaining to be sorted that occupy the
// rest of the list. Initially, the sorted sublist is empty and the unsorted
// sublist is the entire input list. The algorithm proceeds by finding the
// smallest (or largest, depending on sorting order) element in the unsorted
// sublist, exchanging it with the leftmost unsorted element (putting it in
// sorted order), and moving the sublist boundaries one element to the right.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Selection_sort)

package main

import "fmt"

func selectionSort(list []int) {
	for l := list; len(l) > 0; l = l[1:] {
		min := 0

		for idx := range l {
			if l[idx] < l[min] {
				min = idx
			}
		}

		l[0], l[min] = l[min], l[0]
	}
}

func main() {
	l := []int{}
	selectionSort(l)
	fmt.Println(l)

	l = []int{2}
	selectionSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	selectionSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	selectionSort(l)
	fmt.Println(l)
}
