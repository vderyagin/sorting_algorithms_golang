// Shellsort
//
// Time complexity:
//     Depends on gaps sequence
//
// Space complexity:
//     constant (in-place)
//
// Stability:
//     no
//
// Shellsort generalizes an exchanging sort by starting the comparison and
// exchange of elements with elements that are far apart before finishing with
// neighboring elements.
//
// © Wikipedia (http://en.wikipedia.org/wiki/Shell_sort)

package main

import "fmt"

var gaps = []int{3905, 2161, 929, 505, 209, 109, 41, 19, 5, 1}

func insertionSortIdxs(list, idxs []int) {
	for i := 1; i < len(idxs); i++ {
		for j := i - 1; j >= 0; j-- {
			if list[idxs[j]] > list[idxs[j+1]] {
				list[idxs[j]], list[idxs[j+1]] = list[idxs[j+1]], list[idxs[j]]
			}
		}
	}
}

func sortWithGap(list []int, gap int) {
	for i := 0; i < gap; i++ {
		idxs := make([]int, 0)

		for j := i; j < len(list); j += gap {
			idxs = append(idxs, j)
		}

		insertionSortIdxs(list, idxs)
	}
}

func shellSort(list []int) {
	for _, gap := range gaps {
		if gap < len(list) {
			sortWithGap(list, gap)
		}
	}
}

func main() {
	l := []int{}
	shellSort(l)
	fmt.Println(l)

	l = []int{2}
	shellSort(l)
	fmt.Println(l)

	l = []int{2, 1}
	shellSort(l)
	fmt.Println(l)

	l = []int{4, 3, 2, 6, 7, 4, 3, 2, 4, 6, 7, 5}
	shellSort(l)
	fmt.Println(l)
}
